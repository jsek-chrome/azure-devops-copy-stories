import { defineConfig } from 'unocss/vite';
import { presetAttributify, presetIcons, presetUno, transformerDirectives } from 'unocss';

export default defineConfig({
  presets: [
    //_
    presetUno(),
    presetAttributify(),
    presetIcons(),
  ],
  transformers: [
    //_
    transformerDirectives(),
  ],
});
