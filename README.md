# Azure DevOps - Copy stories

Browser Extension to let users of Azure DevOps copy the list of stories for sharing progress during showcase.

![screenshot](./docs/example_1.png)

## Features

- Injects a popup with convinient \[Copy\] button next to the list of stories for every Release.

## Build & Run

> If you don't have pnpm installed, run: `npm install -g pnpm`

```sh
bun i
bun run build
bun run dev
```

Then **load extension in browser with the `extension/` folder**.

For Firefox developers, you can run the following command instead:

```bash
bun start:firefox
```

`web-ext` auto reload the extension when `extension/` files changed.

> While Vite handles HMR automatically in the most of the case, [Extensions Reloader](https://chrome.google.com/webstore/detail/fimgfedafeadlieiabdeeaodndnlbhid) is still recommanded for cleaner hard reloading.

## Publish

After running `pnpm build` & `pnpm pack` upload `extension.crx` or `extension.xpi` to appropriate extension store.

## Credits

Based on template [vitesse-webext](https://github.com/antfu/vitesse-webext)