import type { App } from 'vue';

export function setupApp(app: App) {
  // Inject a globally available `$app` object in template
  app.config.globalProperties.$app = {
    context: '',
  };

  // Provide access to `app` in script setup with `const app = inject('app')`
  app.provide('app', app.config.globalProperties.$app);

  // Install additional plugins for all contexts: popup, options page and content-script.
  // example: if (context !== 'content-script') app.use(i18n)
}
