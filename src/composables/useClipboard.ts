import { useSupported } from '@vueuse/core';
import type { Ref } from 'vue-demi';
import { computed, ref } from 'vue-demi';

interface Options {
  source: Ref<HTMLElement | null>;
  legacy?: boolean;
}

export function useClipboard(options: Options) {
  const { source, legacy = true } = options;

  const isClipboardApiSupported = useSupported(() => navigator && 'clipboard' in navigator);
  const isSupported = computed(() => isClipboardApiSupported.value || legacy);
  const copied = ref(false);

  async function copy() {
    if (source.value == null) {
      console.warn('[Azure DevOps - Copy stories] Target element not found');
      return;
    }

    if (isSupported.value) {
      if (isClipboardApiSupported.value) {
        try {
          const blob = new Blob([source.value.innerHTML], { type: 'text/html' });
          const input = new ClipboardItem({ 'text/html': blob });
          await navigator.clipboard.write([input]);
        } catch (err) {
          console.error('[Azure DevOps - Copy stories] Failed to copy stories', err);
        }
      } else {
        legacyCopy();
      }

      copied.value = true;
      setTimeout(() => copied.value = false, 1500);
    }
  }

  function legacyCopy() {
    window.getSelection()!.removeAllRanges();
    const range = document.createRange();
    range.selectNode(source.value!);
    window.getSelection()!.addRange(range);

    document.execCommand('copy');
    window.getSelection()!.removeAllRanges();
  }

  return {
    isSupported,
    copied,
    copy,
  };
}
