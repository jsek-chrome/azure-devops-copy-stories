import { watchThrottled } from "@vueuse/core";

export function useRouteQuery(key: string) {
  const value: Ref<string | null> = ref(null);

  function refreshValue() {
    const queryString = window.location.search;
    const parameters = new URLSearchParams(queryString);
    value.value = parameters.get(key);
  }

  let previousUrl = ref('');

  const observer = new MutationObserver(() => {
    if (window.location.href !== previousUrl.value) {
      previousUrl.value = window.location.href;
    }
  });

  onMounted(() => {
    observer.observe(document, { subtree: true, childList: true });
    refreshValue();
  });

  onBeforeUnmount(() => {
    observer.disconnect();
  });

  watchThrottled(previousUrl, refreshValue, { throttle: 300 });

  return value;
}
